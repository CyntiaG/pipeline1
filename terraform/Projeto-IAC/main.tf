terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "lab-terraform.tfstate-remote"
    key    = "terraform.tfstate"
  }
}

# Provedor AWS
provider "aws" {
  region = var.aws_region
}


