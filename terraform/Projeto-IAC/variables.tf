# Variável para região
variable "aws_region" {
  type        = string
  description = "Variável para região da AWS. Substitua para a região desejada."
  default     = "us-east-1"
}

# Variável para AMI
variable "instance_ami" {
  type        = string
  description = "Variável para AMI. Substitua pelo ID da AMI desejada"
  default     = "ami-007855ac798b5175e"
}

# Variável para tipo de instância
variable "instance_type" {
  type        = string
  description = "Variável para tipo de instância. Substitua pelo tipo de instância desejada."
  default     = "t2.micro"
}

# Variável para número de instâncias
variable "instance_count" {
  type        = number
  description = "Variável para número de instâncias. Substitua pelo número de instânciaa desejada."
  default     = 1
}

# Variável para nome de bucket
variable "aws_bucket_name" {
  type        = string
  description = "Variável para mudar nome de bucket. Substitua pelo nome desejado."
  default     = "Novo_bucket_S3_pipeline1"
}

# Variável para ACL
variable "aws_acl" {
  type        = string
  description = "Variável para mudar privacidade de ACL. Substitua pelo tipo desejado."
  default     = "private"
}

# Variável para nome da SSH key
variable "aws_SSH_name" {
  type        = string
  description = "Variável para mudar o nome da chave SSH pública. Substitua pelo nome de chave desejado."
  default     = "Chave1"
}


